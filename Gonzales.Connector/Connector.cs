﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using Gonzales.Core;
using System.IO;

namespace Gonzales.Connector
{
    public class Connector : IDisposable
    {
        string _remoteHost;
        int _port;
        TcpClient _client;

        public Connector(string remoteHost, int port)
        {
            _remoteHost = remoteHost;
            _client = new TcpClient();
            _client.Connect(_remoteHost, _port);
            Task.Run(() => Read());
        }

        private void Read()
        {
            while (_client.Available > 0 && _client.Connected)
            {
                var msg = new Message().ReceiveMessage(_client.GetStream());
                msg.Action(msg.Payload);
            }
        }

        public void Publish<T>(string topic, T message) where T : class
        {
            var msg = new Message()
            {
                Topic = topic,
                Operation = Operation.Publish                
            };
            msg.Payload = msg.SerializeDataToBinary(message);
            if (_client.Connected)
                msg.SendMessage(_client.GetStream());
        }

        public void Subscribe(string topic, Action<byte[]> action)
        {
            var message = new Message()
            {
                Topic = topic,
                Action = action,
                Operation = Operation.Subscribe
            };
            if (_client.Connected)
                message.SendMessage(_client.GetStream());
        }                   
        

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
