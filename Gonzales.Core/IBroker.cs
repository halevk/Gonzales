﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gonzales.Core
{
    public interface IBroker
    {
        void Publish<T>(string topic, T message);
        void Subscribe<T>(string topic, Action<T> action);
    }
}
