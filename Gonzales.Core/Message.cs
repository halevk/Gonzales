﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Gonzales.Core
{
    public class Message
    {
        public string Topic { get; set; }
        public Operation Operation { get; set; }
        public Action<byte[]> Action { get; set; }
        public byte[] Payload { get; set; }  
        
        public byte[] SerializeDataToBinary(object data)
        {
            var formatter = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                formatter.Serialize(ms, data);
                ms.Position = 0;
                return ms.ToArray();
            }
        }

        private Message GetMessage(MemoryStream ms)
        {
            ms.Position = 0;
            var formatter = new BinaryFormatter();
            return (Message)formatter.Deserialize(ms);
        }

        public void SendMessage(Stream stream)
        {
            using (stream)
            {
                var data = SerializeDataToBinary(this);
                stream.Write(data, 0, data.Length);
            }
        } 


        public Message ReceiveMessage(Stream streamToRead)
        {
            using (streamToRead)
            {
                using (var ms = new MemoryStream())
                {
                    var buffer = new byte[1024];
                    var count = 0;
                    while ((count = streamToRead.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, count);
                    }
                    return GetMessage(ms);
                }

            }
        }



    }

    public enum Operation
    {
        Subscribe,
        Publish,
        UnSubscribe
    }
}
