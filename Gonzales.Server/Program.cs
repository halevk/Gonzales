﻿using Gonzales.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Gonzales.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = 0;
            if (args.Length == 0)
                Console.WriteLine("lütfen bir port numarası belirtin");
            else if (!int.TryParse(args[0], out port))
                Console.WriteLine("port numarası 1024 - 65536 arasında kulanılımaya portlardan olmalıdır");
            else if (port == 0)
                Console.WriteLine("port numarası sıfır (0) olamaz");
            else
            {
                var server = new Server();
                server.Start(port);
                Console.WriteLine("server listenin on port " + port + " ....");
                Console.Read();
            }

            Console.WriteLine("çıkmak için bir tuşa basınız ...");
            Console.ReadKey();
        }
    }

    public class Server
    {
        TcpListener _listener;
        List<Channel> _channels;
        public async void Start(int portNo)
        {
            _channels = new List<Channel>();
            _listener = new TcpListener(IPAddress.Any,portNo);
            _listener.Start(5);
            while (_listener.Pending())
            {
                var client = await _listener.AcceptTcpClientAsync();
                var channel = new Channel() { Client = client };
                channel.Listen();
                _channels.Add(channel);
            }            
        }       
    }

    public class Channel
    {
        public Channel()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; private set; }
        public TcpClient Client { get; set; }

        public void Listen()
        {            
            while (true)
            {
                if (Client.Available > 0)
                    new Message().ReceiveMessage(Client.GetStream());
            }
        }

        public void SendMessage(Message msg)
        {            
            msg.SendMessage(Client.GetStream());
        }
        
    }

}
